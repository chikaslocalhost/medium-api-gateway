FROM openjdk:8-jdk-alpine
LABEL maintainer="sendtoclabz@gmail.com"
VOLUME /tmp
ADD build/libs/medium-api-gateway-0.0.1-SNAPSHOT.jar medium-api-gateway.jar
EXPOSE 3000
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","/medium-api-gateway.jar"]
